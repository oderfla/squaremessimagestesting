//
//  ViewController.m
//  squareMessImagesTesting
//
//  Created by Alfredo Haddad on 28/06/2015.
//  Copyright (c) 2015 Alfredo Haddad. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    currentPos = 0;
    
    [self loadImages];
    [self setUpGameView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadImages {
    
    images = [[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"levels" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSData *_fileData = [[NSData alloc] init];
    _fileData = [myJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *localError = nil;
    
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:_fileData options:0 error:&localError];
    
    for (NSDictionary *level in parsedObject){
        
        [images addObject:[level objectForKey:@"image"]];
    }
}

- (void) setUpGameView {
    
    originalParts = [[NSMutableArray alloc] init];
    imageParts = [[NSMutableArray alloc] init];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    UIImageView *originalImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:images[currentPos]]];
    
    float reductionPercentage = 100 - ((screenSize.width*100)/originalImage.frame.size.width);
    reductionPercentage = reductionPercentage/100;
    
    float height = originalImage.frame.size.height - (originalImage.frame.size.height*reductionPercentage);
    
    reducedImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, screenSize.width, height)];
    reducedImage.image = originalImage.image;
    reducedImage.contentMode = UIViewContentModeScaleAspectFit;
    reducedImage.hidden = YES;
    
    [self.view addSubview:reducedImage];
    
    dimensions = [self getDimensions:@"2"];
    
    float block_width = screenSize.width / dimensions;
    float block_height = reducedImage.frame.size.height / dimensions;
    float x = 0;
    float y = reducedImage.frame.origin.y;
    
    for (int i =0; i < pow(dimensions, 2); i++) {
        
        CGImageRef topImgRef = CGImageCreateWithImageInRect([reducedImage.image CGImage], CGRectMake(x, y, block_width, block_height));
        UIImage* subimage = [UIImage imageWithCGImage:topImgRef];
        
        UIImageView *imageToShow = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, block_width, block_height)];
        imageToShow.image = subimage;
        imageToShow.alpha = 0;
        
        [originalParts addObject:imageToShow];
        
        if ((i+1) % dimensions == 0) {
            
            x=0;
            y = y + block_height;
        }
        else{
            x = x + block_width;
        }
    }
    
    imageParts = [originalParts mutableCopy];
    [self shuffleAndDraw];
}

- (int) getDimensions:(NSString*) difficulty {
    
    int numberOfSquares = 0;
    
    if ([difficulty isEqualToString:@"0"]) {
        return 4;
    }
    if ([difficulty isEqualToString:@"1"]) {
        return 5;
    }
    if ([difficulty isEqualToString:@"2"]) {
        return 6;
    }
    
    return numberOfSquares;
}

- (void) shuffleAndDraw {
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    float block_width = screenSize.width / dimensions;
    float block_height = reducedImage.frame.size.height / dimensions;
    float x = 0;
    float y = reducedImage.frame.origin.y;
    
    for (int i = 0; i < imageParts.count; ++i) {
        
        int r = (arc4random() % (int)[imageParts count]);
        [imageParts exchangeObjectAtIndex:i withObjectAtIndex:r];
    }
    
    for (int i = 0; i < imageParts.count; ++i) {
        
        UIImageView *imageToShow = imageParts[i];
        imageToShow.tag = -1;
        imageToShow.frame = CGRectMake(x, y, block_width, block_height);
        
        [self.view addSubview:imageToShow];
        
        if ((i+1) % dimensions == 0) {
            
            x=0;
            y = y + block_height;
        }
        else{
            x = x + block_width;
        }
    }
    
    [self showImageSquares:0];
}

- (void) showImageSquares:(int) pos {
    
    int cont = pos;
    if (cont < imageParts.count) {
        
        [UIView animateWithDuration:0.05 animations:^{
            
            UIImageView *v = imageParts[cont];
            v.alpha = 1;
            
        } completion:^(BOOL finished) {
            
            [self showImageSquares:cont+1];
        }];
    }
}

- (IBAction)goRight:(id)sender{

    int aux = currentPos+1;
    
    if (aux < images.count) {
        
        for (UIView* b in self.view.subviews)
        {
            if (![b isKindOfClass:[UIButton class]]) {
                [b removeFromSuperview];
            }
        }
        
        currentPos =  currentPos + 1;
        [self setUpGameView];
    }
}

- (IBAction)goLeft:(id)sender{

    int aux = currentPos-1;
    
    if (aux >= 0) {
        
        for (UIView* b in self.view.subviews)
        {
            if (![b isKindOfClass:[UIButton class]]) {
                [b removeFromSuperview];
            }
        }
        
        currentPos =  currentPos - 1;
        [self setUpGameView];
    }
}

@end
