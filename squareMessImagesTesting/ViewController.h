//
//  ViewController.h
//  squareMessImagesTesting
//
//  Created by Alfredo Haddad on 28/06/2015.
//  Copyright (c) 2015 Alfredo Haddad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {

    NSMutableArray *images;
    UIImageView *reducedImage;
    
    int dimensions;
    
    int currentPos;
    
    NSMutableArray *imageParts;
    NSMutableArray *originalParts;
}

@end

