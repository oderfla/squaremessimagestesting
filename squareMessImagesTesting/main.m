//
//  main.m
//  squareMessImagesTesting
//
//  Created by Alfredo Haddad on 28/06/2015.
//  Copyright (c) 2015 Alfredo Haddad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
